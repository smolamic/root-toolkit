#include <iostream>
#include "test/common.h"
#include "Filelist.h"

using namespace rtk;

int testBasic() {
    Filelist l("test/testfile.root");

    TFile *f;
    TRY(f = l.at(0));
    ASSERT(f != nullptr);
    ASSERT(f->IsOpen());

    return 0;
}

int main(int argc, char** argv) {
    CHECK(testBasic);
    return 0;
}
