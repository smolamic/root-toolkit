#include <iostream>
#include <sstream>
#include "docopt.cpp/docopt.h"
#include "hmath.h"
#include "test/common.h"
#include "common.h"
#include "usage.h"

using namespace rtk;
using ArgMap = std::map<std::string, docopt::value>;

int testWithError() {
    std::stringstream ss;
    ArgMap args = docopt::docopt(
            usage,
            {"integral", "--nlerror", "test/simple.root/histo"},
            true
            );

    TRY(integral(args, ss));

    std::string value, error;
    ss >> value;
    ss >> error;
    ASSERT(std::stod(value) > 1.99);
    ASSERT(std::stod(value) < 2.01);
    ASSERT(std::stod(error) > 1.4);
    ASSERT(std::stod(error) < 1.5);
    return 0;
}

int main(int args, char** argv) {
    CHECK(testWithError);
    return 0;
}
