#include <iostream>
#include <sstream>
#include "ls.h"
#include "test/common.h"

using namespace rtk;

static const char usage[] =
R"(Root Toolkit

    Usage:
        rtk ls [-dl] <path>

    Options:
        -d --directory          List only directories.
        -l --long               Show more info.
)";

int testBasic() {
    const char* argv[] = {"rtk", "ls", "test/2d.root"};
    std::map<std::string, docopt::value> args
        = docopt::docopt(usage, { argv + 1, argv + 3 }, true, "0.0.1");

    std::ostringstream os;
    ls(args, os);

    ASSERT(os.str() == "FakeEfficiency2D_el_pt_dr \tRealEfficiency2D_el_pt_dr \t"
            "FakeEfficiency2D_mu_pt_dr \tRealEfficiency2D_mu_pt_dr\n");
    return 0;
}

int main(int argc, char** argv) {
    CHECK(testBasic);
    return 0;
}
