#include <iostream>
#include <sstream>
#include <sys/stat.h>
#include "docopt.cpp/docopt.h"
#include "hmath.h"
#include "test/common.h"
#include "common.h"
#include "usage.h"

using namespace rtk;
using ArgMap = std::map<std::string, docopt::value>;

int testAddAndSave() {
    ArgMap args = docopt::docopt(
            usage,
            {"add", "-s", "test/added.root/added_eff",
            "test/2d.root/FakeEfficiency2D_el_pt_dr",
            "test/2d.root/RealEfficiency2D_el_pt_dr"},
            true
            );

    std::ostringstream os;
    add(args, os);
    ASSERT(os.str() == "test/added.root/added_eff\n");
    struct stat buffer;
    ASSERT(stat("test/added.root", &buffer) == 0);
    return 0;
}



int main(int args, char** argv) {
    CHECK(testAddAndSave);
    return 0;
}
