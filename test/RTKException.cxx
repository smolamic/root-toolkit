#include <string>
#include <stdexcept>
#include "test/common.h"
#include "RTKException.h"

using namespace rtk;

int testBasic() {
   RTKException exc;
   return 0;
}

int testFromLiteral() {
    RTKException exc("abc");
    ASSERT(std::string("abc") == exc.what());
    return 0;
}

int testFromStream() {
    RTKException exc;
    exc << "abc";
    ASSERT(std::string("abc") == exc.what());

    ASSERT(std::string("abcdef") == (RTKException("abc") << std::string("def")).what());

    ASSERT(std::string("abcdefghi") == (RTKException("abc") << "def" << "ghi").what());

    return 0;
}

int testCopy() {
    RTKException exc("abc");
    RTKException other(exc);
    ASSERT(std::string("abc") == other.what());
    return 0;
}

int testThrow() {
    try {
        throw RTKException("abc");
    } catch (RTKException &exc) {
        ASSERT(std::string("abc") == exc.what());
    }

    try {
        throw RTKException("def") << "ghi";
    } catch (std::exception &exc) {
        ASSERT(std::string("defghi") == exc.what());
    }

    return 0;
}

int main(int argc, char** argv) {
    CHECK(testBasic);
    CHECK(testFromLiteral);
    CHECK(testFromStream);
    CHECK(testCopy);
    CHECK(testThrow);
    return 0;
}
