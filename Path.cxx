#include <boost/algorithm/string.hpp>
#include "RTKException.h"
#include "Filelist.h"
#include "Path.h"

namespace rtk {

void Path::init(std::string& input) {
    boost::algorithm::trim(input);
    size_t split = input.find(".root");
    if (split != std::string::npos) {
        split += 5;
    } else {
        split = input.find(".txt");

        if (split == std::string::npos)
            throw RTKException("Path Error:\n")
                << "Paths must be of the form "
                << "\"path/to/rootfile(.root|.txt)[/path/to/TObject]\".\n"
                << "No \".root\" or \".txt\" file ending found.\n"
                << "Path: \"" << input << "\".";

        split += 4;
    }

    m_file = input.substr(0, split);

    if (split == input.length())
        return;

    if (input.at(split) != '/')
        throw RTKException("Path Error:\n")
            << "Paths must be of the form "
            << "\"path/to/rootfile(.root|.txt)[/path/to/TObject]\".\n"
            << "First character after filepath must be \"/\".";

    m_object = input.substr(split + 1);
}

Path::Path(std::string& input) {
    init(input);
}

Path::Path(const std::string& input) {
    std::string inpath(input);
    init(inpath);
}

Path::Path(const char* input) {
    std::string inpath(input);
    init(inpath);
}

const char* Path::getObject() {
    if (hasObject())
        return m_object.c_str();

    throw RTKException("Path Error:\n")
        << "Path does not specify Object.";
}

Filelist Path::getFilelist() {
    return Filelist(getFile());
}

TH1* Path::getHistogram() {
    Filelist files(getFile());

    TH1* histo;
    try {
        histo = files.hadd(getObject());
    } catch (RTKException& e) {
        throw RTKException("Path::getHistogram failed: ") << e.what();
    }
    if (!histo)
        throw RTKException("Failed to get histogram \"") 
            << getObject() << "\" from file \"" << getFile() << "\".";
    histo->SetDirectory(0);
    return histo;
}

}
