#include <stdexcept>
#include <string>
#include "TH2.h"
#include "Filelist.h"
#include "common.h"
#include "Path.h"
#include "hmath.h"
#include "RTKException.h"

namespace rtk {

using ArgMap = std::map<std::string, docopt::value>;

void add(const ArgMap args, std::ostream& os) {
    try {
        TH1* histo = Path(pipeArg(args.at("<path>").asString())).getHistogram();
        if (args.at("--title"))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            TH1* other = Path(otherpath).getHistogram();
            histo->Add(other);
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo, outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Add failed: ") << e.what();
    }
}

void sub(const ArgMap args, std::ostream& os) {
    try {
        TH1* histo = Path(pipeArg(args.at("<path>").asString())).getHistogram();
        if (args.at("--title"))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            TH1* other = Path(otherpath).getHistogram();
            histo->Add(other, -1);
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo, outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Subtract failed: ") << e.what();
    }
}

void mul(const ArgMap args, std::ostream& os) {
    try {
        TH1* histo = Path(pipeArg(args.at("<path>").asString())).getHistogram();
        if (args.at("--title"))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            TH1* other = Path(otherpath).getHistogram();
            histo->Multiply(other);
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo, outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Multiply failed: ") << e.what();
    }
}

void div(const ArgMap args, std::ostream& os) {
    try {
        TH1* histo = Path(pipeArg(args.at("<path>").asString())).getHistogram();
        if (args.at("--title"))
            histo->SetTitle(args.at("--title").asString().c_str());

        for (auto &otherpath : args.at("<other>").asStringList()) {
            TH1* other = Path(otherpath).getHistogram();
            histo->Divide(other);
        }

        if (args.at("--color")) {
            Color_t color = args.at("--color").isLong()
                ? args.at("--color").asLong()
                : getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo, outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Divide failed: ") << e.what();
    }
}

void scale(const ArgMap args, std::ostream& os) {
    try {
        TH1* histo = Path(pipeArg(args.at("<path>").asString())).getHistogram();

        double factor = 1;
        for (auto &f : args.at("<factor>").asStringList())
            factor *= std::stod(f);

        histo->Scale(factor);

        for (auto& outpath : getArg("--save", args).asStringList())
            os << write(histo, outpath, getArg("--recreate", args).asBool()) << std::endl;
        if (args.at("--save").asStringList().empty())
            os << cache(hash(args, 8).c_str(), histo) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Scale failed: ") << e.what();
    }
}

double getObjectEntries(TObject* obj, const ArgMap args) {
        if (obj->InheritsFrom("TTree")) {
            auto tree = (TTree*)obj;
            if (args.at("--fast").asBool())
                return tree->GetEntriesFast();
            else if (args.at("--selection"))
                return tree->GetEntries();
            else 
                return tree->GetEntries(args.at("--selection").asString().c_str());
        } else if (obj->InheritsFrom("TH1")) {
            auto histo = (TH1*)obj;
            if (args.at("--effective").asBool())
                return histo->GetEffectiveEntries();
            else
                return histo->GetEffectiveEntries();
        } else {
            throw RTKException("Cannot get entries for object of type ")
                << obj->ClassName();
        }
}

void entries(const ArgMap args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>").asString()));
        Filelist files(path.getFile());

        double e = getObjectEntries(files.get(path.getObject()), args);

        for (auto &o : args.at("<more>").asStringList()) {
            Path morepath(o);
            Filelist morefiles(path.getFile());
            e += getObjectEntries(morefiles.get(path.getObject()), args);
        }

        os << e << std::endl;

    } catch (std::exception &e) {
        throw RTKException("RTK Entries failed: ") << e.what();
    }
}

void integral(const ArgMap args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>").asString()));
        TH1* histo = path.getHistogram();

        for (auto &o : args.at("<more>").asStringList()) {
            Path morepath(o);
            TH1* other = morepath.getHistogram();
            histo->Add(other);
        }

        Int_t min = args.at("--inclusive").asBool() ? 1 : 0;
        Int_t max = args.at("--inclusive").asBool() ? histo->GetNbinsX() + 1 : histo->GetNbinsX();

        if (args.at("--pmerror").asBool()) {
            Double_t error;
            Double_t value = histo->IntegralAndError(min, max, error);
            os << value << "+-" << error << std::endl;
        } else if (args.at("--nlerror").asBool()) {
            Double_t error;
            Double_t value = histo->IntegralAndError(min, max, error);
            os << value << std::endl << error << std::endl;
        } else {
            os << histo->Integral(min, max) << std::endl;
        }
    } catch (std::exception &e) {
        throw RTKException("RTK Integral failed: ") << e.what();
    }
}

void sumweights(const ArgMap args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>").asString()));
        TH1* histo = path.getHistogram();
        double sw = histo->GetSumOfWeights();

        if (args.at("--inclusive").asBool()) {
            if (histo->InheritsFrom("TH2"))
                throw RTKException("Inclusive SumWeights for 2D histograms is not supported yet.");
            sw += histo->GetBinContent(0);
            sw += histo->GetBinContent(histo->GetNbinsX() + 1);
        }

        for (auto &o : args.at("<more>").asStringList()) {
            Path morepath(o);
            TH1* other = morepath.getHistogram();
            sw += other->GetSumOfWeights();

            if (args.at("--inclusive").asBool()) {
                if (histo->InheritsFrom("TH2"))
                    throw RTKException("Inclusive SumWeights for 2D histograms is not supported yet.");
                sw += histo->GetBinContent(0);
                sw += histo->GetBinContent(histo->GetNbinsX() + 1);
            }
        }

        os << sw << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK SumWeights failed: ") << e.what();
    }
}

void projection(const ArgMap args, std::ostream& os) {
    try {
        Path path(pipeArg(args.at("<path>").asString()));
        TH2* histo = (TH2*)path.getHistogram();

        TH1D* p;

        const std::string axis = args.at("--axis").asString();
        if (axis == "x") {
            p = histo->ProjectionX();
        } else if (axis == "y") {
            p = histo->ProjectionY();
        } else {
            throw RTKException("Projections may be created only over axis x or y, ")
                << "\"" << axis << "\" given.";
        }

        os << cache(hash(args, 8).c_str(), p) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Projection failed: ") << e.what();
    }
}

void set(const ArgMap args, std::ostream& os) {
    try {
        TH1* histo = Path(pipeArg(getArg("<path>", args).asString())).getHistogram();

        if (getArg("--title", args))
            histo->SetTitle(args.at("--title").asString().c_str());

        if (getArg("--color", args)) {
            Color_t color = getColor(args.at("--color").asString());
            histo->SetLineColor(color);
            histo->SetFillColor(color);
            histo->SetMarkerColor(color);
        }

        os << cache(hash(args, 8).c_str(), histo) << std::endl;
    } catch (std::exception &e) {
        throw RTKException("RTK Set failed: ") << e.what();
    }
}

}
