#ifndef RTK_FILELIST_H
#define RTK_FILELIST_H

#include "TTree.h"
#include "TH1.h"
#include "TFile.h"
#include "TChain.h"

namespace rtk {

class Filelist {
public:
    Filelist(const char *input);
    Filelist(const std::string& input);
    ~Filelist();

    void add(const std::string input);

    TChain* getChain(const char *name);
    TH1* hadd(const char *path);
    TFile* at(const size_t index);
    TObject* get(const char *path);

private:
    void open(const size_t index);
    void openAll();
    std::vector<std::string> m_filenames;
    std::vector<TFile*> m_files;
};

}
#endif // RTK_FILELIST_H
