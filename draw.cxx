#include <memory>
#include <limits>
#include <boost/algorithm/string.hpp>
#include "TH1.h"
#include "THStack.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TRatioPlot.h"
#include "TPaveText.h"
#include "TLatex.h"
#include "docopt.h"
#include "Filelist.h"
#include "Path.h"
#include "common.h"
#include "draw.h"
#include "RTKException.h"

namespace rtk {

const std::string makeLabel(const TH1* histo) {
    std::ostringstream label;
    label << histo->GetTitle() << "  " << histo->Integral();
    return label.str();
}


void draw(const std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        std::vector<std::string> paths;
        std::vector<std::string> stackpaths;

        {
            auto current = &paths;
            for (const std::string& path : getArg("<histo>", args).asStringList()) {
                if (path == "stack") {
                    current = &stackpaths;
                    continue;
                }
                current->emplace_back(path);
            }
        }

        if (paths.empty() && stackpaths.empty()) {
            paths.emplace_back();
            std::cin >> paths.back();
        }

        if (getArg("--debug", args).asBool()) {
            std::cerr << "Draw ";
            for (auto path : paths)
                std::cerr << path << " ";
            std::cerr << "stack ";
            for (auto path : stackpaths)
                std::cerr << path << " ";
            std::cerr << std::endl;
        }


        TApplication* app = nullptr;
        if (!getArg("--batch", args).asBool())
            app = new TApplication("RTK Draw", 0, nullptr);

        if (getArg("--legend", args).asBool()) gStyle->SetOptStat(0);
        gStyle->SetOptTitle(0);
        gStyle->SetTextFont(42);
        gStyle->SetMarkerSize(1.2);
        gStyle->SetMarkerStyle(20);
        gStyle->SetLineWidth(2.);

        TCanvas canvas("RTK Draw", "RTK Draw", 600, 800);
        TVirtualPad* pad = canvas.cd();


        if (getArg("--logx", args).asBool())
            canvas.SetLogx();
        if (getArg("--logy", args).asBool())
            canvas.SetLogy();
        if (getArg("--logz", args).asBool())
            canvas.SetLogz();

        if (getArg("--palette", args))
            gStyle->SetPalette(getPalette(getArg("--palette", args).asString()));

        const std::string stackopt = "HIST";
        const std::string samestackopt = "HIST SAME";
        const std::string histopt = getArg("--style", args)
            ? args.at("--style").asString()
            : "E1";
        const std::string sameopt(histopt + " SAME");
        double xmin = std::numeric_limits<double>::max();
        double xmax = std::numeric_limits<double>::min();
        double ymax = std::numeric_limits<double>::min();
        double ymin = std::numeric_limits<double>::max();
        TH1* main = nullptr;
        THStack* stack = nullptr;
        std::vector<TH1*> other;
        std::string xlabel, ylabel, zlabel;

        if (!stackpaths.empty()) {
            stack = new THStack();

            for (auto &path : stackpaths) {
                TH1* histo = Path(path).getHistogram();   
                histo->SetLineColor(1);
                stack->Add(histo);
                xmin = std::min(xmin, histo->GetXaxis()->GetXmin());
                xmax = std::max(xmax, histo->GetXaxis()->GetXmax());
                if (xlabel.empty())
                    xlabel = histo->GetXaxis()->GetTitle();
            }

            ymax = std::max(ymax, stack->GetMaximum());
            ymin = std::min(ymin, stack->GetMinimum());
        }


        for (auto &path : paths) {
            TH1* histo = Path(path).getHistogram();
            if (main == nullptr) {
                main = histo;
                if (xlabel.empty())
                    xlabel = histo->GetXaxis()->GetTitle();
                if (histo->InheritsFrom("TH2"))
                    ylabel = histo->GetYaxis()->GetTitle();
            }
            else other.push_back(histo);
            xmin = std::min(xmin, histo->GetXaxis()->GetXmin());
            xmax = std::max(xmax, histo->GetXaxis()->GetXmax());
            ymax = std::max(ymax, histo->GetMaximum());
            ymin = std::min(ymin, histo->GetMinimum());
        }

        if (getArg("--ymax", args)) ymax = std::stod(args.at("--ymax").asString());
        if (getArg("--ymin", args))
            ymin = std::stod(args.at("--ymin").asString());
        else if (ymin == 0.)
            ymin = std::max(1., ymax * 0.0001);
        else 
            ymin = std::max(ymin, ymax * 0.0001);

        if (getArg("--ratio", args).asBool()) {
            if (stack == nullptr)
                throw RTKException("Cannot draw ratio plot without a stack as reference");
            if (!other.empty())
                throw RTKException("Ratio may be drawn only for exactly one ")
                    << "main histogram + stack";

            TList *stackHists = stack->GetHists();
            auto* stackSum = (TH1*)stackHists->At(0)->Clone();
            stackSum->Reset();
            for (auto* histo : *(stack->GetHists()))
                stackSum->Add((TH1*)histo);

            auto* ratio = new TRatioPlot(main, stackSum, "divsymm");
            ratio->Draw();
            ratio->SetUpBottomMargin(0.);
            TPad* upper = ratio->GetUpperPad();
            pad = upper->cd();
            stackSum->Delete();
            main->SetDrawOption(histopt.c_str());


            ratio->GetLowerRefGraph()->SetMinimum(0.5);
            ratio->GetLowerRefGraph()->SetMaximum(1.5);
            ratio->GetLowYaxis()->SetNdivisions(8);

            stack->Draw(samestackopt.c_str());

            pad->SetTopMargin(0.06);
        } else {
            if (stack != nullptr) {
                stack->Draw(stackopt.c_str());
                if (main != nullptr) main->Draw(sameopt.c_str());
            } else {
                main->Draw(histopt.c_str());
            }
            for (auto& o : other)
                o->Draw(sameopt.c_str());

            pad->SetTopMargin(0.05);
        }

        Float_t leftMargin = pad->GetLeftMargin();
        Float_t rightMargin = pad->GetRightMargin();
        Float_t topMargin = 0.05;

        canvas.cd();

        if (getArg("--legend", args).asBool()) {
            auto* leg = new TLegend(
                    0.51,
                    0.8,
                    1 - rightMargin - 0.005,
                    1 - topMargin - 0.005,
                    "",
                    "nbNDC"
                    );
            leg->SetBorderSize(0);
            leg->SetFillStyle(0);
            const char* lstyle = args.at("--style") ? "lpf" : "lp";
            if (main != nullptr) leg->AddEntry(main, makeLabel(main).c_str(), lstyle);
            for (auto* histo : other)
                leg->AddEntry(histo, makeLabel(histo).c_str(), lstyle);
            
            if (stack != nullptr) {
                TList* histos = stack->GetHists();
                for (auto* iter = stack->GetHists()->MakeReverseIterator(); iter->Next(); )
                    leg->AddEntry(**iter, makeLabel((TH1*)**iter).c_str(), "f");
            }
            leg->SetY1(std::max(0.5, 1 - topMargin - 0.005 - (leg->GetNRows() * 0.05)));
            leg->Draw();

            if (getArg("--logy", args).asBool())
                ymax *= std::pow(ymax / ymin, 0.4);
            else
                ymax += ((ymax - ymin) / 2.);
        } else {
            if (getArg("--logy", args).asBool())
                ymax *= std::pow(ymax / ymin, 0.1);
            else
                ymax *= 1.1;
        }

        if (args.at("--ymin") || args.at("--logy").asBool()) {
            if (stack != nullptr) stack->SetMinimum(ymin);
            if (main != nullptr) main->SetMinimum(ymin);
        }
        if (getArg("--xmin", args)) xmin = std::stod(args.at("--xmin").asString());
        if (getArg("--xmax", args)) xmax = std::stod(args.at("--xmax").asString());
        if (ylabel.empty()) ylabel = "Entries";
        else zlabel = "Entries";
        if (stack != nullptr) {
            stack->SetMaximum(ymax);
            stack->GetXaxis()->SetLimits(xmin, xmax);
            stack->GetXaxis()->SetTitle(xlabel.c_str());
            stack->GetYaxis()->SetTitle(ylabel.c_str());
        }
        if (main != nullptr) {
            main->SetMaximum(ymax);
            main->GetXaxis()->SetLimits(xmin, xmax);
            main->SetXTitle(xlabel.c_str());
            main->SetYTitle(ylabel.c_str());
            if (!zlabel.empty()) main->SetZTitle(zlabel.c_str());
        }

        Double_t labelOffset = 0;
        if (getArg("--atlas", args).asBool()) {
            std::string text;
            if (getArg("--preliminary", args).asBool()) text = "Preliminary";
            if (getArg("--work-in-progress", args).asBool()) text = "Work in Progress";

            Double_t atlasLeft = leftMargin + 0.02;
            Double_t atlasTop = 1 - topMargin - 0.05;

            TLatex atlas;
            atlas.SetNDC();
            atlas.SetTextFont(72);
            atlas.DrawLatex(atlasLeft, atlasTop, "ATLAS");

            if (!text.empty()) {
                double delx = 100. / pad->GetWw();
                TLatex extra;
                extra.SetTextFont(42);
                extra.SetTextSize(0.04);
                extra.DrawLatexNDC(atlasLeft + delx, atlasTop, text.c_str());
            }

           labelOffset = 50. / pad->GetWh(); 
        }

        if (getArg("--title", args) || !getArg("--text", args).asStringList().empty()) {
            if (args.at("--debug").asBool())
                std::cerr << "Add Title " << args.at("--title").asString() << std::endl;

            auto* label = new TPaveText(
                    leftMargin + 0.005,
                    0.8,
                    0.5,
                    1 - topMargin - 0.008 - labelOffset,
                    "nbNDC"
                    );

            label->SetTextAlign(11);

            if (args.at("--title")) 
                label->AddText(args.at("--title").asString().c_str());

            if (getArg("--energy", args)) {
                std::string energy("#sqrt{S} = ");
                energy += args.at("--energy").asString();
                label->AddText(energy.c_str());
            }

            if (getArg("--lumi", args)) {
                std::string lumi("L = ");
                lumi += args.at("--lumi").asString();
                label->AddText(lumi.c_str());
            }

            for (auto& text : getArg("--text", args).asStringList())
                label->AddText(text.c_str());

            label->SetY1(
                    std::max(
                        0.4,
                        std::min(
                            1 - topMargin - 0.08,
                            1 - topMargin - 0.005 - labelOffset
                            - (label->GetSize() * 0.04)
                            )
                        )
                    );

            label->SetFillStyle(0);
            label->SetBorderSize(0);
            label->Draw();
        }

        canvas.Update();

        // save or run
        for (auto& filepath : getArg("--save", args).asStringList())
            canvas.SaveAs(filepath.c_str());

        if (!getArg("--batch", args).asBool()) {
            canvas.Draw();
            app->Run();
            delete app;
        }

    } catch (std::exception &e) {
        throw RTKException("RTK Draw failed:\n") << e.what();
    }
}

}
