# root-toolkit

A toolkit to handle root files unix-style in the command line

## usage
Root Toolkit
```
    Usage:
        rtk [--debug] ls [-dl] <path>
        rtk [--debug] draw [-s <oufile> --palette <palette> --style <style> --legend --batch --title <title> --logx --logy --logz] [<histo>...|-] [stack <histo>...]
        rtk [--debug] print [<path>|-]
        rtk [--debug] fill [-n --selection <selection> --scale <scaleexp>... --title <title> --color <color> --nentries <nentries>] [--nbins <nbins> --min <min> --max <max>] <varexp> [<path>|-] [<more>...]
        rtk [--debug] fill [-n --selection <selection> --scale <scaleexp>... --title <title> --color <color> --nentries <nentries>] [--nbinsx <nbinsx> --xmin <xmin> --xmax <xmax> --nbinsy <nbinsy> --ymin <ymin> --ymax <ymax>] <varexp> [<path>|-] [<more>...]
        rtk [--debug] add [--title <title> --color <color>] [<path>|-] <other>...
        rtk [--debug] sub [--title <title> --color <color>] [<path>|-] <other>...
        rtk [--debug] mul [--title <title> --color <color>] [<path>|-] <other>...
        rtk [--debug] div [--title <title> --color <color>] [<path>|-] <other>...
        rtk [--debug] save [<source>|-] <target>
        rtk [--debug] scale [<path>|-] <factor>...
        rtk [--debug] sum [--type <type>] <branch> [<path>|-] [<more>...]
        rtk [--debug] entry [--type <type>] <entry> <branch> [<path>|-]
        rtk [--debug] entries [--effective|--fast|--selection <selection>] [<path>|-] [<more>...]
        rtk [--debug] integral [--inclusive (--pmerror|--nlerror)] [<path>|-] [<more>...]
        rtk [--debug] sumweights [--inclusive] [<path>|-] [<more>...]
        rtk [--debug] projection --axis <axis> [<path>|-]
        rtk -h|--help
        rtk --version

    Options:
        -d --directory              List only directories.
        -l --long                   Show more info.
        -b --batch                  Run in batch mode.
        -s --save <outfile>         Save Drawn histogram to OUFILE instead of displaying it.
           --selection <selection>  Event Selection
           --scale <scaleexp>       Scale factor
           --title <title>          Histogram title
           --nbins <nbins>          Number of bins for 1D histogram
           --min <min>              Minimum for 1D histogram
           --max <max>              Maximum for 1D histogram
           --nbinsx <nbinsx>        Number of bins on x-axis for 2D histogram
           --xmin <xmin>            Minimum on x-axis for 2D histogram
           --xmax <xmax>            Maximum on x-axis for 2D histogram
           --nbinsy <nbinsy>        Number of bins on y-axis for 2D histogram
           --ymin <ymin>            Minimum on y-axis for 2D histogram
           --ymax <ymax>            Maximum on y-axis for 2D histogram
           --color <color>          Set Line, Fill and marker color
           --palette <palette>      Palette to draw histogram colors from
           --style <style>          Drawing Style for histograms
           --legend                 Add Legend to histogram
           --type <type>            Branch Type [default: double]
           --nentries <nentries>    Set Maximum Entries to run over
        -n --no-cache               Ignore Cache
           --effective              Return effective entries for histograms
           --fast                   Use GetEntriesFast on TTrees
           --inclusive              Include overflow bins
           --logx                   Log scale for x-axis
           --logy                   Log scale for y-axis
           --logz                   Log scale for z-axis
           --axis <axis>            Which axis to project to
           --pmerror                Output error as 'value+-error'
           --nlerror                Output error on new line ('value\nerror')
           --debug                  Print debug output
        -h --help                   Show this screen.
           --version                Show rtk version and exit.
```

Commands can be piped together, for example
```
rtk fill "jet_pt[0]" test/testfile.root/nominal_Loose | rtk draw --logx
```

## dependencies
* Make sure to have root set up correctly
* Either clone with `git clone --recurse-submodules` or run
`git submodule init --update` after cloning

## build
`make`

## test
`make test`

## run
`./bin/rtk`

You probably want to add root-toolkit/bin to your PATH.

