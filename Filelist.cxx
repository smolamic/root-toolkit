#include <fstream>
#include <vector>
#include <boost/algorithm/string.hpp>
#include "Filelist.h"
#include "RTKException.h"

namespace rtk {

Filelist::Filelist(const char* input) {
    add(input);
}

Filelist::Filelist(const std::string& input) {
    add(input);
}

void Filelist::add(const std::string input) {
    if (boost::ends_with(input, ".root")) {
        m_filenames.push_back(input);
        m_files.push_back(nullptr);
        return;
    }

    if (!boost::ends_with(input, ".txt"))
        throw RTKException(
                "Input must be either single root file or list of root-files in .txt format"
                );

    std::ifstream infile(input.c_str());
    for (std::string line; getline(infile, line); ) {
        boost::trim(line);
        if (line.empty()) continue;
        m_filenames.push_back(line);
    }

    if (m_filenames.empty())
        throw RTKException("Input list is empty");

    m_files.resize(m_filenames.size(), nullptr);
}

Filelist::~Filelist() {
    for (auto &file : m_files) {
        if (file == nullptr || !file->IsOpen()) continue;
        file->Close();
        file = nullptr;
    }
}


TFile* Filelist::at(const size_t index) {
    if (index >= m_filenames.size()) {
        throw RTKException()
            << "Out of bounds error: Trying to access file "
            << index
            << " but this filelist only has "
            << m_filenames.size()
            << " files.";
    }


    open(index);

    return m_files.at(index);
}

TH1* Filelist::hadd(const char *path) {
    try {
        openAll();
    } catch (RTKException& e) {
        throw RTKException("Filelist::hadd failed: ") << e.what();
    }

    auto it = m_files.cbegin();
    TH1* histo = (TH1*)(*it)->Get(path);
    if (!histo)
        throw RTKException("Failed to get \"") << path << "\" from file.";
    histo->Sumw2();
    it++;
    for (; it != m_files.cend(); it++) {
        TH1* next = (TH1*)(*it)->Get(path);
        if (!next)
            throw RTKException("Failed to get \"") << path << "\" from file.";
        histo->Add(next);
    }
    return histo;
}

TObject* Filelist::get(const char *path) {
    open(0);
    auto it = m_files.cbegin();
    TObject* obj = (*it)->Get(path);
    it++;
    if (!obj)
        throw RTKException("Failed to get \"") << path << "\" from file.";

    if (obj->InheritsFrom("THStack")) {
        if (m_filenames.size() > 1)
            throw RTKException("Cannot load THStack from more than one file.");
        return obj;
    } else if (obj->InheritsFrom("TH1")) {
        TH1* histo = (TH1*)obj;
        histo->Sumw2();
        openAll();
        for (; it != m_files.cend(); it++) {
            TH1* next = (TH1*)(*it)->Get(path);
            if (!next)
                throw RTKException("Failed to get \"") << path << "\" from file.";
            histo->Add(next);
        }
        return histo;
    } else if (obj->InheritsFrom("TTree")) {
        (*it)->Close();
        m_files.at(0) = nullptr;
        return getChain(path);
    }

    throw RTKException("Type of object at \"") << path << "\" is not supported yet."; 
}

TChain* Filelist::getChain(const char *path) {
    auto *chain = new TChain(path);

    for (auto filename : m_filenames)
        chain->Add(filename.c_str());

    return chain;
}

void Filelist::open(const size_t index) {
    if (m_files.at(index) != nullptr && m_files.at(index)->IsOpen()) return;

    m_files.at(index) = TFile::Open(m_filenames.at(index).c_str());

    if (!m_files.at(index)->IsOpen())
        throw RTKException()
            << "Failed to open TFile "
            << m_filenames.at(index)
            << ".";
}

void Filelist::openAll() {
    for (size_t index = 0; index < m_filenames.size(); ++index)
        open(index);
}

}
