#ifndef RTK_PATH_H
#define RTK_PATH_H

#include <string>
#include "TH1.h"
#include "Filelist.h"

namespace rtk {

class Path {
public:
    Path(std::string& input);
    Path(const std::string& input);
    Path(const char *input);
    ~Path() {}

    const char* getFile() { return m_file.c_str(); }
    const char* getObject();
    bool hasObject() { return !m_object.empty(); }

    TH1* getHistogram();
    Filelist getFilelist();
private:
    void init(std::string& input);
    std::string m_file;
    std::string m_object;
};

}

#endif // RTK_PATH_H
