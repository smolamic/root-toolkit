#include <stdexcept>
#include <string>
#include <boost/algorithm/string/split.hpp>
#include "Filelist.h"
#include "common.h"
#include "Path.h"
#include "save.h"
#include "RTKException.h"

namespace rtk {
void save(const std::map<std::string, docopt::value> args, std::ostream& os) {
    try {
        Path sourcepath(pipeArg(args.at("<source>").asString()));
        Filelist files(sourcepath.getFile());

        TH1* histo = files.hadd(sourcepath.getObject());

        os << write(histo, getArg("<target>", args).asString(), getArg("--recreate", args).asBool())
            << std::endl;

    } catch (std::exception &e) {
        throw RTKException("RTK Save failed: ") << e.what();
    }
}

}
