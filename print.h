#ifndef RTK_PRINT_H
#define RTK_PRINT_H

#include <sstream>
#include <iostream>
#include "TH1.h"
#include "TH2.h"
#include "docopt.h"

namespace rtk {

int print(const std::map<std::string, docopt::value> args, std::ostream& os = std::cout);

int print1(TH1* histo, std::ostream& os = std::cout);
int print2(TH2* histo, std::ostream& os = std::cout);

}

#endif // RTK_PRINT_H
