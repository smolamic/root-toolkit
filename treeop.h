#ifndef RTK_TREEOP_H
#define RTK_TREEOP_H

#include <iostream>
#include <sstream>
#include "docopt.h"

namespace rtk {

void fill(const std::map<std::string, docopt::value> args, std::ostream& os = std::cout);

void sum(const std::map<std::string, docopt::value> args, std::ostream& os = std::cout);

void entry(const std::map<std::string, docopt::value> args, std::ostream& os = std::cout);

}

#endif // RTK_TREEOP_H
