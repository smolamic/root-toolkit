
using namespace rtk;

static const char usage[] =
R"(Root Toolkit

    Usage:
        rtk [--debug] ls [-dl] <path>
        rtk [--debug] draw [-s <outfile>... --palette <palette> --style <style> --legend --batch --title <title> --logx --logy --logz --ratio --xmin <xmin> --xmax <xmax> --ymin <ymin> --ymax <ymax> --text <text>... --atlas (--preliminary|--work-in-progress) --lumi <lumi> --energy <energy>] [<histo>...|-] [stack <histo>...]
        rtk [--debug] print [<path>|-]
        rtk [--debug] fill [-n --selection <selection> --scale <scaleexp>... --title <title> --color <color> --nentries <nentries> -s <outfile>... --recreate] [--nbins <nbins> --min <min> --max <max>] <varexp> [<path>|-] [<more>...]
        rtk [--debug] fill [-n --selection <selection> --scale <scaleexp>... --title <title> --color <color> --nentries <nentries> -s <outfile>... --recreate] [--nbinsx <nbinsx> --xmin <xmin> --xmax <xmax> --nbinsy <nbinsy> --ymin <ymin> --ymax <ymax>] <varexp> [<path>|-] [<more>...]
        rtk [--debug] add [--title <title> --color <color> -s <outfile>... --recreate] [<path>|-] <other>...
        rtk [--debug] sub [--title <title> --color <color> -s <outfile>... --recreate] [<path>|-] <other>...
        rtk [--debug] mul [--title <title> --color <color> -s <outfile>... --recreate] [<path>|-] <other>...
        rtk [--debug] div [--title <title> --color <color> -s <outfile>... --recreate] [<path>|-] <other>...
        rtk [--debug] save [--recreate] [<source>|-] <target>
        rtk [--debug] scale [-s <outfile>... --recreate] [<path>|-] <factor>...
        rtk [--debug] sum [--type <type>] <branch> [<path>|-] [<more>...]
        rtk [--debug] entry [--type <type>] <entry> <branch> [<path>|-]
        rtk [--debug] entries [--effective|--fast|--selection <selection>] [<path>|-] [<more>...]
        rtk [--debug] integral [--inclusive (--pmerror|--nlerror)] [<path>|-] [<more>...]
        rtk [--debug] sumweights [--inclusive] [<path>|-] [<more>...]
        rtk [--debug] projection --axis <axis> [<path>|-]
        rtk [--debug] set [--color <color> --title <title>] [<path>|-]
        rtk -h|--help
        rtk --version

    Options:
        -d --directory              List only directories.
        -l --long                   Show more info.
        -b --batch                  Run in batch mode.
        -s --save <outfile>...      Save Drawn histogram to outfile
           --selection <selection>  Event Selection
           --scale <scaleexp>       Scale factor
           --title <title>          Histogram title
           --nbins <nbins>          Number of bins for 1D histogram
           --min <min>              Minimum for 1D histogram
           --max <max>              Maximum for 1D histogram
           --nbinsx <nbinsx>        Number of bins on x-axis for 2D histogram
           --xmin <xmin>            Minimum on x-axis
           --xmax <xmax>            Maximum on x-axis
           --nbinsy <nbinsy>        Number of bins on y-axis for 2D histogram
           --ymin <ymin>            Minimum on y-axis
           --ymax <ymax>            Maximum on y-axis
           --color <color>          Set Line, Fill and marker color
           --palette <palette>      Palette to draw histogram colors from
           --style <style>          Drawing Style for histograms
           --legend                 Add Legend to histogram
           --type <type>            Branch Type [default: double]
           --nentries <nentries>    Set Maximum Entries to run over
        -n --no-cache               Ignore Cache
           --effective              Return effective entries for histograms
           --fast                   Use GetEntriesFast on TTrees
           --inclusive              Include overflow bins
           --logx                   Log scale for x-axis
           --logy                   Log scale for y-axis
           --logz                   Log scale for z-axis
           --axis <axis>            Which axis to project to
           --pmerror                Output error as 'value+-error'
           --nlerror                Output error on new line ('value\nerror')
           --ratio                  Add ratio plot
           --text <text>            Add test to plots
           --atlas                  Add ATLAS-Logo
           --preliminary            Add Preliminary label
           --work-in-progress       Add Work in Progress label
           --lumi <lumi>            Set Luminosity
           --energy <energy>        Set centre-of-mass energy
           --recreate               Recreate file when saving
           --debug                  Print debug output
        -h --help                   Show this screen.
           --version                Show rtk version and exit.
)";

