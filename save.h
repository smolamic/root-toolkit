#ifndef RTK_SAVE_H
#define RTK_SAVE_H

#include <iostream>
#include <sstream>
#include "docopt.h"

namespace rtk {

void save(const std::map<std::string, docopt::value> args, std::ostream& os = std::cout);

}

#endif // RTK_SAVE_H
